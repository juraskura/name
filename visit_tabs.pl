use Selenium::Firefox;
use Data::Dumper;

my ($item, $helper);
my $driver = Selenium::Firefox->new;

# wait for pages (graphs) to load
# usage: $driver->pause($pause);
my $pause = 1000;

# load the instance (change the URL to fit your needs)
$driver->get('http://10.22.22.10/lpar2rrd/');
$helper = $driver->get_title();
print "$helper\n";
$driver->pause($pause);

sub visit {
  # expand all folders in menu tree
  $driver->find_element('//*[@id="expandall"]')->click();
  print "expanded\n";
  $driver->pause($pause);

  # explicitly open the Dashboard
  eval { my @top_nodes = $driver->find_elements('//span[@class="fancytree-title"]'); };
  my $dashboard = $top_nodes[0];
  $dashboard->click();
  my $dashboard_label = $dashboard->get_text();
  print "opened $dashboard_label\n";
  $driver->pause($pause);

  # now it should be possible to get all nodes, not just the top-level ones
  my $nodes = $driver->find_elements('//span[@class="fancytree-title"]');

  foreach my $node (@{$nodes}) {
    my $node_label = $node->get_text();
    print "node: $node_label\n";

    # click the item and wait for a while
    $node->click();
    $driver->pause($pause);

    # workaround in case the item was a folder
    $driver->find_element('//*[@id="expandall"]')->click();
    $driver->pause($pause);

    # walk through the item's tabs
    my $tabs = $driver->find_elements('//a[@class="ui-tabs-anchor"]');
    foreach my $tab (@{$tabs}) {
      my $tab_label = $tab->get_text();
      print "tab: $tab_label\n";

      # click the tab and wait for a while
      # eval as an workaround, because some tabs cannot be clicked and throw an error:
      # "element not interactable X could not be scrolled into view"
      eval { $tab->clicka(sdfasd) };
                        if ($@) {print "Haha\n";}
      $driver->pause($pause);
    }
  }
}

visit();

$driver->shutdown_binary;
